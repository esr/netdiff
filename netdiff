#! /bin/sh
# netdiff - diff, but one or both arguments can be scp-style host:path pairs
#
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: Copyright Eric S. Raymond <esr@thyrsus.com>

filtered=""
labelcount=0
argcount=0
for arg in "$@"
do
    argcount=$((argcount+1))
    if [ "${arg}" = "--label" ]
    then
	labelcount=$((labelcount+1))
    fi
    if [ "${argcount}" = "$(($#-1))" ]
    then
	a_whole="${arg}"
    elif [ "${argcount}" = "$#" ]
    then
	b_whole="${arg}"
    else
	filtered="${filtered} ${arg}"
    fi
done
a_host=$(expr "${a_whole}" : '\(.*\):')
b_host=$(expr "${b_whole}" : '\(.*\):')
if [ "${a_host}" = "" ]
then
    a_path="${a_whole}"
else
    a_path="${a_whole##*:}"
fi
if [ "${b_host}" = "" ]
then
    b_path="${b_whole}"
else
    b_path="${b_whole##*:}"
fi

trap 'rm -fr /tmp/netdiff$$-a_copy /tmp/netdiff$$-b_copy' EXIT HUP INT QUIT TERM

netcopy () {
    # Eligible transport commands most preserve file modtimes
    # and recursively copy directories.
    scp -q -r -p "$1:$2" "$3"
    # This almost works, but its interactive
    # messages seem to be impossible to shut up.
    # Note that unlike scp -r it does not chase
    # symlinks.
    #echo "get -fpr $2 $3" | sftp -b - -q "$1"
}

# If either netcopy operation craps out, die cleanly.
set -e

# This code relies on the fact that stat -c "%Y" produces ISO timestamps
# in exactly the same format as the timestamp part of diff -y labels.
if [ "$a_host" = "" ]
then
    a_local="${a_path}"
else
    a_local=/tmp/netdiff$$-a_copy
    netcopy "${a_host}" "${a_path}" "${a_local}"
fi
if [ "$b_host" = "" ]
then
    b_local="${b_path}"
else
    b_local=/tmp/netdiff$$-b_copy
    netcopy "${b_host}" "${b_path}" "${b_local}"
fi

# Theoretically unsafe: filenames containing % could confuse it.
# So could file content exactly matching the tempfile names,
# shellcheck disable=SC2086,SC2090
diff ${filtered} "${a_local}" "${b_local}" | sed \
    -e "s%/tmp/netdiff$$-a_copy%${a_whole}%" \
    -e "s%/tmp/netdiff$$-b_copy%${b_whole}%"

#end
